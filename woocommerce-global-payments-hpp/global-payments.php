<?php
/*
 * Plugin Name: WooCommerce Global Payments Payment Gateway
 * Plugin URI: https://gitlab.com/
 * Description: Global Payments Payment gateway for woocommerce
 * Author: yhw
 * Author URI: /
 * Version: 1.0.0
 */

add_action('plugins_loaded', 'woocommerce_global_payments_init', 0);
function woocommerce_global_payments_init() {
	if(!class_exists('WC_Payment_Gateway')) return;
	
	class WC_Global_Payments extends WC_Payment_Gateway {
		public $response_endpoint = 'wc_global_payments';

		public function __construct() {
			
			$this->id = 'global_payments';
			$this->medthod_title = 'GlobalPayments';
			$this->has_fields = false;

			$this->init_form_fields();
			$this->init_settings();

			$this->title = $this->settings['title'];
			$this->description = $this->settings['description'];
			$this->sandbox_mode = $this->settings['sandbox_mode'];
			$this->merchant_id = $this->settings['merchant_id'];
			$this->sub_account = $this->settings['sub_account'];
			$this->auto_settle = $this->settings['auto_settle'];
			$this->response_endpoint = $this->settings['response_endpoint'];
			$this->status_api = $this->settings['status_api'];
			$this->secure_hash_secret = empty($this->settings['secure_hash_secret']) ? $this->generateSecret() : $this->settings['secure_hash_secret'];
			$this->payment_method = $this->settings['payment_method'];
			$this->curr_code = get_woocommerce_currency();
			$this->prefix = $this->settings['prefix'];

			$this->msg['message'] = "";
			$this->msg['class'] = "";

			if ( version_compare( WOOCOMMERCE_VERSION, '2.0.0', '>=' ) ) {
				add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( &$this, 'process_admin_options' ) );
			} else {
				add_action( 'woocommerce_update_options_payment_gateways', array( &$this, 'process_admin_options' ) );
			}
			add_action('woocommerce_receipt_global_payments', array(&$this, 'receipt_page'));
			
			/* for callback/datafeed */
			add_action( 'woocommerce_api_'.$this->settings['status_api'], array( $this, 'gateway_response' ) );
		}

		public function init_form_fields() {
			$this->form_fields = array(
				'enabled'=>array(
					'title'=>__('Enable/Disable', 'globalpayments'),
					'type'=>'checkbox',
					'label'=>__('Enable Global Payments', 'globalpayments'),
					'default'=>'no',
				),
				'title'=>array(
					'title'=>__('Title:', 'globalpayments'),
					'type'=> 'text',
					'description'=>__('This controls the title which the user sees during checkout.', 'globalpayments'),
					'default'=>__('GlobalPayments', 'globalpayments'),
					'desc_tip'=>true,
				),
				'description'=>array(
					'title'=>__('Description:', 'globalpayments'),
					'type'=>'text',
					'description'=>__('This controls the description which the user sees during checkout.', 'globalpayments'),
					'default'=>__('Pay securely through GlobalPayments Secure Servers.', 'globalpayments'),
					'desc_tip'=>true,
				),
				// Advanced options
				'advanced_options'=>array(
					'title'=>__('Advanced options', 'globalpayments'),
					'type'=>'title',
				),
       				'sandbox_mode'=>array(
					'title'=>__('Global Payments sandbox', 'globalpayments'),
					'type'=>'checkbox',
					'label'=>__( 'Enable Global Payments sandbox', 'globalpayments' ),
					'default'=>'no',
					'description'=>sprintf(__('Global Payments sandbox can be used to test payments. Check <a href="%s">Global Payments developer</a>.', 'globalpayments'), 'https://developer.globalpay.com/'),
				),
				'merchant_id'=>array(
					'title'=>__('Merchant ID', 'globalpayments'),
					'type'=>'text',
					'description'=>__('Your Client ID assigned by Global Payments.', 'globalpayments'),
				),
				'sub_account'=>array(
					'title'=>__('Account', 'globalpayments'),
					'type'=>'text',
					'description'=>__('The sub-account for this request to be processed through.', 'globalpayments'),
				),
				'auto_settle'=>array(
					'title'=>__('AutoSettle/AutoCapture', 'globalpayments'),
					'type'=>'select',
					'description'=>__("Used to specify the settlement/capture type. Allowed values:<br/>
0 - Delayed Settle (Capture) / Authorize, don't automatically add to the settlement file.<br/>
1 - Auto Settle (Capture) / Charge, automatically added to the next settlement file.<br/>
MULTI - Multi Settle (Capture), don't automatically add to the settlement file. Enables multiple settlement/capture requests up to 115% of the original transaction value.", 'globalpayments'),
					'options'=>array(
						'0'=>__('Delayed Settle', 'globalpayments'),
						'1'=>__('Auto Settle', 'globalpayments'),
						'MULTI'=>__('Multi Settle', 'globalpayments'),
					),
					'default'=>__('1', 'globalpayments'),
				),
				'response_endpoint'=>array(
					'title'=>__('Order received endpoint', 'globalpayments'),
					'type'=>'text',
					'description'=>__('Used to set which URL in your application the transaction response will be sent to. A fixed URL can also be added to your account by our support team.', 'globalpayments'),
					'default'=>'order-received',
				),
				'status_api'=>array(
					'title'=>__('Status Update API', 'globalpayments'),
					'type'=>'text',
					'description'=>__('The endpoint which will receive payment status messages. This will include the result of the transaction or any updates to the transaction status.', 'globalpayments'),
					'default'=>'wc_global_payments',
				),
				'secure_hash_secret' => array(
					'title' => __('Secure Hash Secret'),
					'type' => 'text',
					'description' => __('Shared Secret for Global Payments Request Hash'),
					'default' => $this->generateSecret(),
				),
       				'payment_method'=>array(
					'title'=>__('Payment Method', 'globalpayments'),
					'type'=>'select',
					'description'=>__('(Optional) Field which allows you to select payment method(s) to offer the customer, or route customer to a specific one.', 'globalpayments'),
					'options'=>array(
						''=>__('Default all', 'globalpayments'),
						'card'=>__('Credit card', 'globalpayments'),
						'paypal'=>__('PayPal', 'globalpayments'),
						'sofort'=>__('Sofort', 'globalpayments'),
						'testpay'=>__('Test Pay', 'globalpayments'),
						'sepapm'=>__('Sepapm', 'globalpayments'),
					),
					'default'=>__('', 'globalpayments'),
				),
                		'prefix'=>array(
					'title'=>__('Prefix', 'globalpayments'),
					'type'=>'text',
					'description'=>__('(Optional) Prefix for Order Reference No. (Warning: Do not use a dash "-" because the system uses it as a separator between the prefix and the order reference no.)', 'globalpayments'),
					'default'=>__('', 'globalpayments'),
				),
			);
		}

		public function generateSecret() {
			$length = 10;
			return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
		}

		public function get_payment_url() {
			return $this->sandbox_mode === 'no' ?
				'https://pay.realexpayments.com/pay' // live url
				:
				'https://pay.sandbox.realexpayments.com/pay' // test url
			;
		}

		public function admin_options() {
			echo '<h3>'.__('GlobalPayments Payment Gateway Settings', 'globalpayments').'</h3>';
			echo '<hr/>';
			echo '<table class="form-table">';
			// Generate the HTML For the settings form.
			$this->generate_settings_html();
			echo '</table>';

		}

		public function payment_fields() {
			if($this->description) echo wpautop(wptexturize($this->description));
		}

		/**
		 * Receipt Page
		 **/
		public function receipt_page($order) {
			echo '<p>'.__('Thank you for your order. We are now redirecting you to the Payment Gateway to proceed with the payment.', 'globalpayments').'</p>';
			echo $this->generate_global_payments_form($order);
		}
		
		/**
		 * Generate GlobalPayments button link
		 **/
		public function generate_global_payments_form($order_id) {

			global $woocommerce;

			$order = new WC_Order($order_id);
			$billing = $order->get_data()['billing'];
			$date = new DateTime();
                        $timestamp = $date->format('YmdHis');
			
			if($this->prefix == ''){
				$orderRef = $order_id;
			}else{
				$orderRef = $this->prefix . '-' . $order_id;
			}
						
			$recevie_url = esc_url( add_query_arg( 'utm_nooverride', '1', $this->get_return_url( $order ) ) );
			$fail_url = esc_url( $order->get_cancel_order_url() );
			$status_url = esc_url(home_url( '/' ) . 'wc-api/' . $this->settings['status_api']);
			
			$secureHash = $this->generateSecureHash(
				[$timestamp, $this->merchant_id, $orderRef, $this->roundGP($order->get_total()), $this->curr_code],
				$this->secure_hash_secret
			);
					
			$global_payments_args = array(
				'TIMESTAMP'=>$timestamp,
				'MERCHANT_ID'=>$this->merchant_id,
				'ACCOUNT'=>$this->sub_account,
				'ORDER_ID'=>$orderRef,
				'AMOUNT'=>$this->roundGP($order->get_total()),
				'CURRENCY'=>$this->curr_code,
				'SHA1HASH'=>$secureHash,
				'AUTO_SETTLE_FLAG'=>$this->auto_settle,
				'HPP_VERSION'=>2,
				// <!-- APMs Mandatory Fields -->
				'HPP_CUSTOMER_COUNTRY'=>$billing['country'],
				'HPP_CUSTOMER_FIRSTNAME'=>$billing['first_name'],
				'HPP_CUSTOMER_LASTNAME'=>$billing['last_name'],
				'MERCHANT_RESPONSE_URL'=>$recevie_url,
				'HPP_TX_STATUS_URL'=>$status_url,
				// <!-- APMs Mandatory Fields -->
				// <!-- APMs Optional Fields -->
				'PM_METHODS'=>empty($this->payment_method) ? 'cards|paypal|testpay|sepapm|sofort' : $this->payment_method,
				// <!-- End APMs Optional Fields -->
          		);

			$global_payments_args_array = array();
			foreach($global_payments_args as $key=>$value){
				$global_payments_args_array[] = "<input type='hidden' name='$key' value='$value'/>";
			}
			
			return '<form action="' . $this->get_payment_url() . '" method="post" target="iframe" id="global_payments_payment_form">
            				' . implode('', $global_payments_args_array) . '
					<input type="submit" value="Pay with Global Payments">
				</form>
				<script type="text/javascript">
					jQuery(function(){
						//setTimeout("global_payments_payment_form();", 5000);
					});
					function global_payments_payment_form(){
						jQuery("#global_payments_payment_form").submit();
					}
				</script>
			';

		}

		public function generateSecureHash($fields = [], $secret = '') {
			$blueprint = implode('.',$fields);
			return sha1(sha1($blueprint).'.'.$secret);
		}

		// Round number for global payments hpp
		public function roundGP($amount) {
			return round($amount, 0);
		}
		
		/**
		 * Process the payment and return the result
		 **/
		public function process_payment($order_id) {
			$order = new WC_Order($order_id);
			
			return array(
				'result'=>'success',
				'redirect'=>$order->get_checkout_payment_url( true ),
			);
			
		}

		/**
		 * Check for valid global_payments server datafeed
		 **/
		public function gateway_response($api = true) {

			global $woocommerce;

			/*
			Sample HTTP GET https://developer.globalpay.com/hpp/payment-methods#hpp-status-url
			*/

			$orderid = $_POST['ORDER_ID'];
			$sha1hash = $_POST['SHA1HASH'];
			$result = $_POST['RESULT'];
			$timestamp = $_POST['TIMESTAMP'];
			$merchantid = $_POST['MERCHANT_ID'];
			$message = $_POST['MESSAGE'];
			$pasref = $_POST['PASREF'];
			$paymentmethod = empty($_POST['PAYMENTMETHOD']) ? 'cards' : $_POST['PAYMENTMETHOD'];
			
			if ($api) echo "1";

			$order_id = $orderid;

			//prefix handler
			$hasPrefix = preg_match("/-/", $order_id);
			if($hasPrefix == 1) {
				$exploded_order_id = explode("-", $order_id);
				$order_id = $exploded_order_id[1];
			}

			$output = [
				'status'=>false,
				'message'=>'',
			];
			if($order_id != ''){
				$order = new WC_Order($order_id);
				$orderStatus = $order->get_status();

				if($orderStatus != 'completed') {
						
					$secureHash = $this->generateSecureHash(
						[$timestamp, $merchantid, $orderid, $result, $message, $pasref, $paymentmethod],
						$this->secure_hash_secret
					);
					// Check Response Hash
					if ($secureHash === $sha1hash) {
						if($result == "00") {
							// 00, Successfully && finalised
							if(false&&$orderStatus == 'processing'){ // do anyway
								//do nothing
							}else{									
								$this->msg['message'] = 'Thank you for shopping with us. Your account has been charged and your transaction is successful. We will be shipping your order to you soon. Payment reference no: ' . $pasref;
								$this->msg['class'] = 'woocommerce_message';

								//$order->payment_complete();
								$order->update_status('completed');
								$order->add_order_note('Your payment was successful! Payment reference no: '.$pasref);
								$woocommerce->cart->empty_cart();	
								$output['status'] = true;
								$output['message'] = ' - Payment Success!';
							}
						} else if($result == "101") {
							// 101, Did not complete successfully
							if($orderStatus == 'processing'){
								//do nothing
							}else{							
								$this->msg['message'] = 'Thank you for shopping with us. However, the transaction has been declined. Payment reference no: '. $pasref;
								$this->msg['class'] = 'woocommerce_error';

								$order->update_status('failed');
								$order->add_order_note('Sorry! your payment was unsuccessful! Payment reference no: '.$pasref);
								$output['status'] = true;
								$output['message'] = ' - Payment Failed!';
							}
						} else {
							// 01, Pending
							$order->add_order_note('Payment final status will be sent to the Status Update URL! Payment reference no: '.$pasref);
							$output['status'] = true;
							$output['message'] = ' - Status Update URL!';
						}
					} else {
						$this->msg['message'] = 'Security Error. Illegal access detected. Payment reference no: '.$pasref;
						$this->msg['class'] = 'error';

						$order->update_status('failed');
						$order->add_order_note('Secure Hash checking failed! Payment reference no: '.$pasref);
						$output['message'] = ' - Secure Hash Failed!';
					}
					add_action('the_content', array(&$this, 'showMessage'));
				} else {
					$output['message'] = ' - Order already completed!';
				}
			} else {
				$output['message'] = ' - Order not exist!';
			}

			if (!$api) return $output;

			echo $output['message'];
			exit();
			
		}
		public function showMessage($content) {
			return '<div class="box '.$this->msg['class'].'-box">'.$this->msg['message'].'</div>'.$content;
		}
		
/*
		// get all pages
		public function get_pages($title = false, $indent = true) {
			$wp_pages = get_pages('sort_column=menu_order');
			$page_list = array();
			if ($title) $page_list[] = $title;
			foreach ($wp_pages as $page) {
				$prefix = '';
				if ($indent) {
					$has_parent = $page->post_parent;
					while($has_parent) {
						$prefix .=  ' - ';
						$next_page = get_page($has_parent);
						$has_parent = $next_page->post_parent;
					}
				}
				// add to page list array array
				$page_list[$page->ID] = $prefix . $page->post_title;
			}
			return $page_list;
		}
*/
	}

	
	/**
	 * Add the Gateway to WooCommerce
	 **/
	function woocommerce_add_global_payments_gateway($methods) {
		$methods[] = 'WC_Global_Payments';
		return $methods;
	}

	add_filter('woocommerce_payment_gateways', 'woocommerce_add_global_payments_gateway' );
	
}
